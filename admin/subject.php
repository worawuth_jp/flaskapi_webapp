<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <?php
    include '../layout/header.php';
    $_SESSION['menu'] = 'admin/subject';
    ?>
    <link rel="stylesheet" href="../styles/admin.css">
</head>
<body>
<?php include '../layout/navbar.php' ?>
<div class="main">
    <div class="container">
        <div class="col-md-12 text-center p-3">
            <div class="row">
                <div class="input-group mb-3">
                    <input type="text" class="form-control col-md-4 ml-auto" placeholder="ค้นหาอาจารย์"
                           aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary self-btn-primary" type="button"><i
                                    class="fas fa-search"></i></button>
                    </div>
                    <button href="#" class="btn self-btn-primary btn-outline-secondary ml-3" data-toggle="modal"
                            data-target="#add_subject">เพิ่มวิชา
                    </button>

                </div>

            </div>

            <div class="row">
                <table class="table table-striped">
                    <thead class="bg-table-primary">
                    <tr>
                        <th>รหัสวิชา</th>
                        <th>ชื่อวิชา</th>
                        <th>กลุ่ม</th>
                        <th>ภาคการศึกษา</th>
                        <th>ปีการศึกษา</th>
                        <th>สถานะ</th>
                        <th>จัดการ</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $HOST . '/api/subjects',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                    ));

                    $response = curl_exec($curl);

                    curl_close($curl);
                    $dataDecode = json_decode($response);
                    $data = $dataDecode->results;
                    /*print_r($response);
                    print_r($dataDecode);*/
                    if (count($data) == 0) {
                        ?>
                        <tr>
                            <td colspan="7" class="text-center">No Data</td>
                        </tr>
                        <?php
                    }
                    foreach ($data as $value) {

                        ?>
                        <tr>
                            <td class="text-center"><?php echo $value->subject_code ?></td>
                            <td class="text-center"><?php echo $value->subject_name ?></td>
                            <td class="text-center"><?php echo !is_null($value->subject_group) ? $value->subject_group : '-' ?></td>
                            <td class="text-center"><?php echo !is_null($value->subject_term) ? $value->subject_term : '-' ?></td>
                            <td class="text-center"><?php echo !is_null($value->subject_term) ? $value->subject_term : '-' ?></td>
                            <td class="text-center"><?php echo $value->status ? 'เปิด' : 'ปิด' ?></td>
                            <td class="text-center"></td>

                        </tr>
                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <nav aria-label="Page navigation example border">
                <ul class="pagination justify-content-end">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="add_subject" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="background: #ce9f8d">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <div style="min-height: 300px">
                        <h3>+ เพิ่มข้อมูลรายวิชา</h3>

                        <form>
                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    รหัสรายวิชา
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="subject_id" class="form-control col-md-6"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    ชื่อวิชา
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="subject_name" class="form-control col-md-10"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    คำอธิบายรายวิชา
                                </div>
                                <div class="col-md-9">
                                    <textarea name="subject_detail" class="form-control col-md-10"></textarea>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    กลุ่ม
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="subject_group" class="form-control col-md-10"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    หลักสูตร
                                </div>
                                <div class="col-md-9">
                                    <select name="subject_course" class="form-control col-md-10">

                                    </select>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    ปีการศึกษา
                                </div>
                                <div class="col-md-9">
                                    <select name="subject_term" class="form-control col-md-10">

                                    </select>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    สถานะ
                                </div>
                                <div class="col-md-9">
                                    <select name="subject_status" class="form-control col-md-10">

                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="text-center mt-5">
                        <button type="button" class="btn btn-success mr-auto">บันทึก</button>
                        <button type="button" class="btn btn-danger ml-auto" data-dismiss="modal">ยกเลิก</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include '../layout/footer.php' ?>
</body>
</html>