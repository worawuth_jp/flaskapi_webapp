<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <?php
    include '../layout/header.php';
    $_SESSION['menu'] = 'admin/changePassword';
    ?>
    <link rel="stylesheet" href="../styles/admin.css">
</head>
<body >
<?php include '../layout/navbar.php'?>
<div class="main">
    <div class="container">
        <div class="col-md-12 text-center p-3">
            <main style="display: flex;justify-content: center;align-items: center;height: 89vh;">
                <div class="col-md-9 ml-auto mr-auto reset-box p-4">
                    <form class="text-center" action="./send-reset-password.php" method="post">

                        <div class="form-group">
                            <h5 class="p-2">เปลี่ยนรหัสผ่าน</h5>
                        </div>

                        <div class="row col-md-12 mb-2">

                            <div class="col-md-3 text-left">
                                <label for="username" class="col-form-label">รหัสผ่านเดิม : </label>
                            </div>
                            <input type="password" class="form-control col-md-5" id="oldpassword" name="oldpassword" aria-describedby="passwordHelp"  autocomplete="off">

                        </div>

                        <div class="row col-md-12 mb-2">
                            <div class="col-md-3 text-left">
                                <label for="username" class="col-form-label">รหัสผ่านใหม่ : </label>
                            </div>
                            <input type="password" class="form-control col-md-5" id="newpassword" name="newpassword" aria-describedby="passwordHelp"  autocomplete="off">

                        </div>

                        <div class="row col-md-12 mb-2">
                            <div class="col-md-3 text-left">
                                <label for="username" class="col-form-label">รหัสผ่านใหม่อีกครั้ง : </label>
                            </div>
                            <input type="password" class="form-control col-md-5" id="confnewpassword" name="confnewpassword" aria-describedby="passwordHelp"  autocomplete="off">

                        </div>
                        <div class="row mb-2">
                            <button type="submit" class="btn mt-4 btn-success ml-auto">ยืนยัน</button>
                            <button type="button" class="btn mt-4 ml-3 btn-danger mr-auto">ยกเลิก</button>
                        </div>
                    </form>
                </div>
            </main>
        </div>
    </div>
</div>
<?php include '../layout/footer.php'?>
</body>
</html>