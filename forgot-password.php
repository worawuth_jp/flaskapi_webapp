<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>

    <?php include 'layout/header.php'?>
    <link rel="stylesheet" href="styles/login.css">
</head>
<body>
<div class="container justify-center" >
    <main style="display: flex;justify-content: center;align-items: center;height: 99.5vh;">
        <div class="col-md-4 ml-auto mr-auto login-box p-4">
            <form class="text-center">

                <div class="form-group">

                    <h3 class="p-2">ลืมรหัสผ่าน</h3>
                </div>

                <div class="form-group">
                    <input type="email" class="form-control col-md-12" id="email" aria-describedby="EmailHelp" placeholder="Email" autocomplete="off">
                    <!--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                </div>

                <button type="submit" class="btn mt-4 pl-4 pr-4">ส่ง</button>
            </form>
        </div>
    </main>
</div>
<?php include 'layout/footer.php'?>
</body>
</html>