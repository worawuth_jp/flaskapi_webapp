<!-- Just an image -->
<style>
    .self-nav {
        background: #ce9f8d;
        max-height: 80px;
    }

    .self-nav li {
        padding-left: 10px;
        padding-right: 10px;
    }
    .self-nav a  {
        color: #f5f8e6;
        height: auto;
    }

    .self-nav li :hover,.self-nav li :hover  {
        color: #f5f8e6 !important;
        background: #a46f59;
        height: 100%;
    }

    .self-active ,.self-active a{
        color: #f5f8e6 !important;
        background: #a46f59;
    }

    .table tbody tr{
        background: #ebdbd3;
    }

    thead{
        background: #a46f59;
    }

    .self-btn-primary{
        background: #ce9f8d;
        color: black;
    }

    .self-btn-primary:hover{
        background: #a46f59;
        color: #f5f8e6;
    }

    .reset-box{
        background: #ce9f8c;
        border: solid thin #dcd1c8;
        border-radius: 20px;
    }

    .reset-box input{
        background: #f4e5e2;
    }

    .errors{
        color: red;
    }

    .ow-anywhere {
        overflow-wrap: anywhere;
    }

    .ow-break-word {
        word-break:break-word;
        white-space:normal;
    }
</style>
<?php
include '../constants.php';
if(!isset($_SESSION['user_id'])){
    header('location: login.php');
}
?>
<nav class="navbar navbar-expand-lg p-0 pl-2 pr-2 self-nav fixed">
    <a class="navbar-brand" href="./">
        <img src="../images/LOGO3.png" class="img-fluid" width="80" height="70" alt="LOGO">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse p-0" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto ml-auto">

            <?php
            if(!isset($_SESSION['type_user'])){
                header('../login.php');
            }
            if($_SESSION['type_user'] == $TYPE_ADMIN){
                ?>
                <li class="nav-item <?php echo $_SESSION['menu'] == 'admin/index' ? 'self-active' : ''?>">
                    <a class="nav-link self-nav" href="./">หน้าหลัก <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'admin/teacher' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./teacher.php">อาจารย์</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'admin/subject' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./subject.php">วิชา</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'admin/changePassword' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./reset-password.php">เปลี่ยนรหัส</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'signout' ? 'self-active' : ''?>">
                    <a class="nav-link" href="logout.php">ออกจากระบบ</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'admin/profile' ? 'self-active' : ''?>">
                    <a class="nav-link" href="profile.php">
                        <i class="fas fa-user"></i>
                        Admin
                    </a>
                </li>
            <?php
            }
            ?>

            <?php
            if($_SESSION['type_user'] == $TYPE_TEACHER){
                ?>
                <li class="nav-item <?php echo $_SESSION['menu'] == 'teacher/index' ? 'self-active' : ''?>">
                    <a class="nav-link self-nav" href="./">หน้าหลัก <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/student' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./students.php">นักศึกษา</a>
                </li>
                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/database' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./database.php">ฐานข้อมูล</a>
                </li>
                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/test' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./test.php">แบบทดสอบ</a>
                </li>
                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/changePassword' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./reset-password.php">เปลี่ยนรหัส</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'signout' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./logout.php">ออกจากระบบ</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/profile' ? 'self-active' : ''?>">
                    <a class="nav-link" href="#">
                        <i class="fas fa-user"></i>
                        Teacher
                    </a>
                </li>
                <?php
            }
            ?>

            <?php
            if($_SESSION['type_user'] == $TYPE_STUDENT){
                ?>
                <li class="nav-item <?php echo $_SESSION['menu'] == 'student/index' ? 'self-active' : ''?>">
                    <a class="nav-link self-nav" href="./">หน้าหลัก <span class="sr-only">(current)</span></a>
                </li>

                </li>
                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/test' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./reset-password.php">แบบทดสอบ</a>
                </li>
                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/changePassword' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./test.php">เปลี่ยนรหัส</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'signout' ? 'self-active' : ''?>">
                    <a class="nav-link" href="./logout.php">ออกจากระบบ</a>
                </li>

                <li class="nav-item self-nav <?php echo $_SESSION['menu'] == 'teacher/profile' ? 'self-active' : ''?>">
                    <a class="nav-link" href="#">
                        <i class="fas fa-user"></i>
                        Teacher
                    </a>
                </li>
                <?php
            }
            ?>

        </ul>

    </div>
</nav>