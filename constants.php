<?php
$HOST = 'http://localhost:5000';
$API_PATH = '/api';
$BASE_API_PATH = $HOST.$API_PATH;

$DBNAME = "flask_api";

$TYPE_ADMIN = 'admin';
$TYPE_STUDENT = 'student';
$TYPE_TEACHER = 'teacher';

$RESULT_ONLY_VALUE=1;
$RESULT_QUERY_VALUE=2;
$RESULT_ONLY_MSG="ผลลัพธ์เท่านั้น";
$RESULT_QUERY_MSG="คำสั่งและผลลัพธ์";