<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>

    <?php
    include 'layout/header.php';
    $error = false;
    if(isset($_GET['error']))
        $error = True;
    ?>
    <link rel="stylesheet" href="styles/login.css">
</head>
<body>
<div class="container justify-center" >
    <main style="display: flex;justify-content: center;align-items: center;height: 99.5vh;">
        <div class="col-md-4 ml-auto mr-auto login-box p-4">
            <form class="text-center" action="./check_login.php" method="post">

                <div class="form-group">

                   <h3 class="p-2">Login</h3>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control col-md-12" id="username" name="username" aria-describedby="usernameHelp" placeholder="Username" autocomplete="off">
                    <?php
                    if($error){
                        ?>
                        <small id="emailHelp" class="form-text errors " >Username/Password Invalid</small>
                    <?php
                    }
                    ?>

                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    <?php
                    if($error){
                        ?>
                        <small id="emailHelp" class="form-text errors " >Username/Password Invalid</small>
                        <?php
                    }
                    ?>
                </div>

                <div class="form-group">
                    <select name="type_user" class="form-control col-md-12">
                        <option value="admin">admin</option>
                        <option value="student">student</option>
                        <option value="teacher">teacher</option>
                    </select>
                </div>
                <button type="submit" class="btn mt-4">Log in</button>
            </form>
        </div>
    </main>
</div>
<?php include 'layout/footer.php'?>
</body>
</html>