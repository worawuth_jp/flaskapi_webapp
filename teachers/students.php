<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <?php
    include '../layout/header.php';
    $_SESSION['menu'] = 'teacher/student';
    ?>
    <link rel="stylesheet" href="../styles/admin.css">
</head>
<body>
<?php include '../layout/navbar.php' ?>
<?php
if (isset($_POST['submit'])) {
    $curl = curl_init();
    $request_data = array();
    $request_data['student_id'] = $_POST['student_id'];
    $request_data['firstname'] = $_POST['firstname'];
    $request_data['lastname'] = $_POST['lastname'];
    $request_data['prename'] = $_POST['prename'];
    $request_data['email'] = $_POST['email'];
    $request_data['major_id'] = $_POST['major_id'];
    $request_data['subject_id'] = $_SESSION['subject_id'];

    curl_setopt_array($curl, array(
        CURLOPT_URL => $BASE_API_PATH . '/student',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($request_data),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    //echo $response;
    header('location: ./students.php');
}

?>
<div class="main">
    <div class="container">
        <div class="col-md-12 text-center p-3">
            <div class="row">
                <div class="input-group mb-3">
                    <input type="text" class="form-control col-md-4 ml-auto" placeholder="ค้นหา"
                           aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary self-btn-primary" type="button"><i
                                    class="fas fa-search"></i></button>
                    </div>
                    <button href="#" class="btn self-btn-primary btn-outline-secondary ml-3" data-toggle="modal"
                            data-target="#add_student">เพิ่มนักศึกษา
                    </button>

                </div>

            </div>

            <div class="row">
                <table class="table table-striped">
                    <thead class="bg-table-primary">
                    <tr>
                        <th>รหัสนักศึกษา</th>
                        <th>คำนำหน้า</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>เอก</th>
                        <th>กลุ่ม</th>
                        <th>จัดการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $BASE_API_PATH . '/students',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                    ));

                    $response = curl_exec($curl);

                    curl_close($curl);
                    $objParser = json_decode($response);
                    $obj = $objParser->results;
                    //print_r($obj);

                    if (count($obj) <= 0) {
                        ?>
                        <tr>
                            <td colspan="7" class="text-center">No Data</td>
                        </tr>
                        <?php
                    }

                    foreach ($obj as $data) {
                        ?>
                        <tr>
                            <td class="text-center"><?= $data->student_id ?></td>
                            <td class="text-center"><?= $data->prename ?></td>
                            <td class="text-center"><?= $data->firstname ?></td>
                            <td class="text-center"><?= $data->lastname ?></td>
                            <td class="text-center"><?= $data->major_name ?></td>
                            <td class="text-center"><?= !is_null($data->subject->subject_group) ? $data->subject->subject_group : "-" ?></td>

                            <td class="text-center"></td>
                        </tr>
                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <nav aria-label="Page navigation example border">
                <ul class="pagination justify-content-end">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="add_student" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="background: #ce9f8d">
                <form action="?form=submit" method="post">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div style="min-height: 300px">
                            <h3>+ เพิ่มข้อมูลนักศึกษา</h3>
                            <input type="hidden" name="subject_id" value="">
                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    รหัสนักศึกษา
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="student_id" class="form-control col-md-6"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    คำนำหน้า
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="prename" class="form-control col-md-10"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    ชื่อ
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="firstname" class="form-control col-md-10"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    นามสกุล
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="lastname" class="form-control col-md-10"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    Email
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="email" class="form-control col-md-10"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    เอก
                                </div>
                                <div class="col-md-9">
                                    <select name="major_id" class="form-control col-md-10">
                                        <?php

                                        $curl = curl_init();

                                        curl_setopt_array($curl, array(
                                            CURLOPT_URL => $BASE_API_PATH . '/majors',
                                            CURLOPT_RETURNTRANSFER => true,
                                            CURLOPT_ENCODING => '',
                                            CURLOPT_MAXREDIRS => 10,
                                            CURLOPT_TIMEOUT => 0,
                                            CURLOPT_FOLLOWLOCATION => true,
                                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST => 'GET',
                                        ));

                                        $response = curl_exec($curl);

                                        curl_close($curl);
                                        $objParser = json_decode($response);
                                        $objs = $objParser->results;
                                        foreach ($objs as $data) {
                                            ?>
                                            <option value="<?= $data->major_id ?>"><?= $data->major_name ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="text-center mt-5">
                            <button type="submit" name="submit" class="btn btn-success mr-auto">บันทึก</button>
                            <button type="button" class="btn btn-danger ml-auto" data-dismiss="modal">ยกเลิก</button>

                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php include '../layout/footer.php' ?>
</body>
</html>