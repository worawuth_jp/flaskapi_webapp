<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <?php
    include '../layout/header.php';
    $_SESSION['menu'] = 'teacher/database';
    ?>
    <link rel="stylesheet" href="../styles/admin.css">
</head>
<body>
<?php include '../layout/navbar.php' ?>
<?php
if (isset($_POST['submit'])) {
    $curl = curl_init();
    $req = array();
    $req['sql'] = $_POST['SQL'];

    curl_setopt_array($curl, array(
        CURLOPT_URL => $BASE_API_PATH . '/query',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($req),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $objParser = json_decode($response);
    $obj = $objParser->results;
}
?>
<div class="main">
    <div class="container">
        <div class="col-md-12 text-center p-3">
            <div class="mt-4" style="background: #eddad2">
                <div class="modal-body">

                    <div style="min-height: 300px">
                        <h3>+ เพิ่มข้อมูลในฐานข้อมูล</h3>

                        <form action="?form=submit" method="post">
                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    เพิ่มข้อมูลในฐานข้อมูล (SQL)
                                </div>
                                <div class="col-md-9">
                                    <textarea name="SQL" class="form-control col-md-10"></textarea>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" name="submit" class="btn btn-success mr-auto">บันทึก</button>
                                <button type="button" class="btn btn-danger ml-auto">ล้าง</button>

                            </div>
                        </form>


                        <div class="row mb-2">
                            <div class="col-md-2 col-form-label">
                                Database ของคุณ
                            </div>
                            <div class="col-md-9 text-left">

                                <select onchange="showTable()" onmouseup="showTable()" name="db" id="db"
                                        class="form-control col-md-5">
                                    <?php

                                    $curl = curl_init();

                                    curl_setopt_array($curl, array(
                                        CURLOPT_URL => $BASE_API_PATH . '/databases',
                                        CURLOPT_RETURNTRANSFER => true,
                                        CURLOPT_ENCODING => '',
                                        CURLOPT_MAXREDIRS => 10,
                                        CURLOPT_TIMEOUT => 0,
                                        CURLOPT_FOLLOWLOCATION => true,
                                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST => 'GET',
                                    ));

                                    $response = curl_exec($curl);

                                    curl_close($curl);
                                    //echo $response;
                                    $objParser = json_decode($response);
                                    $obj = $objParser->results;
                                    foreach ($obj as $data) {
                                        ?>
                                        <option value="<?= $data ?>"><?= $data ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-2 col-form-label">
                                Result
                            </div>
                            <div class="col-md-9">
                                <div class="col-md-12 overflow-auto"
                                     style="min-height: 250px;max-height: 250px;background: white;overflow-y: auto;">
                                    <table border="1" id="result">
                                        <thead>
                                        <tr>
                                            <th>ตาราง</th>
                                        </tr>
                                        </thead>
                                        <tbody id="result_tbody">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

</div
<?php include '../layout/footer.php' ?>
<script>
    showTable();

    function showTable() {
        let db = document.getElementById('db')

        let requestOptions = {
            method: 'GET',
            mode: 'cors',
            redirect: 'follow'
        };

        fetch("<?php echo $BASE_API_PATH ?>/tables?db=" + db.value, requestOptions)
            .then(response => response.text())
            .then(result => {
                let resultBox = document.getElementById('result_tbody');
                let objParser = JSON.parse(result);
                let obj = objParser.results;
                let html = "";

                obj.forEach((item, index) => {
                    html += "<tr>";
                    html += "<td>";
                    html += item
                    html += "</td>";
                    html += "</tr>";
                })
                resultBox.innerHTML = html;
            })
            .catch(error => console.log('error', error));
    }
</script>
</body>
</html>