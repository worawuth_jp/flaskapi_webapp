<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Teacher</title>
    <?php
    include '../layout/header.php';
    $_SESSION['menu'] = 'index';
    ?>
    <link rel="stylesheet" href="../styles/admin.css">
</head>
<body>
<?php include '../layout/navbar.php'?>
<div class="main">
    <div class="container">
        <div class="col-md-12 text-center p-3">
            <h2>ยินดีต้อนรับ</h2>
        </div>
    </div>
</div>
<?php include '../layout/footer.php'?>
</body>
</html>