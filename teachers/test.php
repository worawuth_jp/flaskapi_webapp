<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <?php
    include '../layout/header.php';
    $_SESSION['menu'] = 'teacher/test';
    ?>
    <link rel="stylesheet" href="../styles/admin.css">
</head>
<body>
<?php include '../layout/navbar.php' ?>
<?php
if (isset($_POST['form'])) {
    $req = array();
    if ($_POST['form'] == 2) {
        $req['question_name'] = $_POST['question_name'];
        $req['question_database'] = $_POST['question_database'];
        $req['num_total'] = $_POST['question_num'];
        $req['question_type'] = $_POST['question_type'];
        if ($req['question_type'] == "S") {
            $req['question_num_n'] = 1;
        }
        $date = new DateTime($_POST['question_end_date']);
        $req['question_end_date'] = $date->format('Y-m-d');
        $req['question_start_time'] = $_POST['start_time_h'] . ':' . $_POST['start_time_m'];
        $req['question_end_time'] = $_POST['end_time_h'] . ':' . $_POST['end_time_m'];
        $req['question_status'] = $_POST['question_status'];
        $_SESSION['req'] = json_encode($req);
    }

    if ($_POST['form'] == "success") {
//        print_r(json_encode($_POST));
//        print_r(json_encode($_SESSION['req']));
        $request = json_decode($_SESSION['req']);
        $request->question_sql_type = $_POST['question_sql_type'];
        $request->question_skill_type = $_POST['question_skill_type'];
        $request->question_answer = $_POST['question_answer'];
        $request->question_title = $_POST['question_title'];
        $request->question_point = $_POST['question_point'];
        $request->question_standard = $_POST['question_standard'];
        $request->subject_id = $_SESSION['subject_id'];
        if ($request->question_type == "S") {
            $request->question_num = $_POST['question_num_n'];
        } else {
            $request->question_num = (int)$request->num_total;
        }
        //print_r(json_encode($request));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $BASE_API_PATH . '/question',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($request),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $resParser = json_decode($response);
        if ($resParser->code == 200) {
            if ($request->question_type == "S") {
                $num = $request->question_num;
                $num = $num + 1;
                if ($request->num_total < $num) {
                    header('location: ./test.php');
                } else {
                    header('location: ./test.php?num=' . $num);
                }

            } else {
                header('location: ./test.php');
            }
        }
    }
    //print_r($req);
    //print_r($_POST);
}
?>
<div class="main">
    <div class="container">
        <div class="col-md-12 text-center p-3">
            <div class="row">
                <div class="input-group mb-3">
                    <input type="text" class="form-control col-md-4 ml-auto"
                           placeholder="ค้นหาจากชื่อแบบทดสอบ,ชื่อฐานข้อมูล"
                           aria-label="Search" aria-describedby="Search" id="textSearch">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary self-btn-primary" type="button"
                                onclick="onClickSearch()"><i
                                    class="fas fa-search"></i></button>
                    </div>
                    <button href="#" class="btn self-btn-primary btn-outline-secondary ml-3" id="add_test_btn"
                            data-toggle="modal"
                            data-target="#add_test_1">เพิ่มแบบทดสอบ
                    </button>

                    <button href="#" class="btn self-btn-primary btn-outline-secondary ml-3" id="add_test_2"
                            data-toggle="modal" style="display: none"
                            data-target="#add_test_2S">เพิ่มแบบทดสอบ2
                    </button>

                    <button href="#" class="btn self-btn-primary btn-outline-secondary ml-3" id="add_test_3"
                            data-toggle="modal" style="display: none"
                            data-target="#add_test_2A">เพิ่มแบบทดสอบ3
                    </button>

                </div>

            </div>

            <div class="row">
                <table class="table table-striped">
                    <thead class="bg-table-primary">
                    <tr>
                        <th>ชื่อแบบทดสอบ</th>
                        <th>ชื่อฐานข้อมูล</th>
                        <th>ข้อที่</th>
                        <th>วันที่กำหนดส่ง</th>
                        <th>เวลาเริ่ม</th>
                        <th>เวลาส่ง</th>
                        <th>สถานะ</th>
                        <th>คะแนน</th>
                        <th>จัดการ</th>
                    </tr>
                    </thead>
                    <tbody id="question_body">
                    <?php
                    $obj = array();
                    if (isset($_GET['search'])) {
                        $textSearch = $_GET['search'];
                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => $BASE_API_PATH . '/questions/search?text=' . urlencode($textSearch),
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
                            CURLOPT_CUSTOMREQUEST => 'GET',
                        ));

                        $response = curl_exec($curl);

                        curl_close($curl);
                        //echo $response;

                        $objParser = json_decode($response);
                        $obj = $objParser->results;
                    } else {
                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => $BASE_API_PATH . '/questions?subject_id=' . $_SESSION['subject_id'],
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'GET',
                        ));

                        $response = curl_exec($curl);

                        curl_close($curl);
                        $objParser = json_decode($response);
                        $obj = $objParser->results;
                    }
                    if (count($obj) <= 0) {
                        ?>
                        <tr>
                            <td colspan="9" class="text-center">No Data</td>
                        </tr>
                        <?php
                    }
                    foreach ($obj as $data) {
                        ?>
                        <tr>
                            <td class="text-center"><?= $data->question_name ?></td>
                            <td class="text-center"><?= $data->question_database ?></td>
                            <td class="text-center"><?= $data->question_num ?></td>
                            <td class="text-center"><?= $data->question_end_date ?></td>
                            <td class="text-center"><?= $data->question_start_time ?></td>
                            <td class="text-center"><?= $data->question_end_time ?></td>
                            <td class="text-center"><?= $data->question_status == 1 ? 'เปิด' : 'ปิด' ?></td>
                            <td class="text-center"><?= $data->question_point ?></td>
                            <td class="text-center">
                                <a class="btn" data-toggle="modal"
                                   data-target="#view_details_<?php echo $data->question_id ?>">
                                    <i class="fas fa-eye"></i>
                                    <i class="fas fa-edit"></i>
                                    <!--    VIEW DETAILS Modals -->
                                    <div class="modal fade" id="view_details_<?= $data->question_id ?>"
                                         tabindex="-1"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content" style="background: #ce9f8d">
                                                <form method="post" action="./test.php">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>

                                                        <div style="min-height: 300px">
                                                            <h3>รายละเอียด</h3>


                                                            <div class="row mb-2">
                                                                <div class="col-md-2 font-bold-weight"
                                                                     style="font-weight: bold">
                                                                    ชื่อแบบทดสอบ :
                                                                </div>
                                                                <div class="col-md-9 text-left">
                                                                    <?php echo $data->question_name ?>
                                                                </div>
                                                            </div>

                                                            <div class="row mb-2">
                                                                <div class="col-md-2 font-bold-weight"
                                                                     style="font-weight: bold">
                                                                    ข้อที่ :
                                                                </div>
                                                                <div class="col-md-9 text-left">
                                                                    <?php echo $data->question_num ?>
                                                                </div>
                                                            </div>

                                                            <div class="row mb-2">
                                                                <div class="col-md-2 font-bold-weight"
                                                                     style="font-weight: bold">
                                                                    โจทย์ :
                                                                </div>
                                                                <div class="text-left col-md-9 ow-break-word">
                                                                    <?php echo $data->question_title ?>
                                                                </div>
                                                            </div>

                                                            <div class="row mb-2">
                                                                <div class="col-md-2 font-bold-weight"
                                                                     style="font-weight: bold">
                                                                    database :
                                                                </div>
                                                                <div class="col-md-9 text-left">
                                                                    <?php echo $data->question_database ?>
                                                                </div>
                                                            </div>

                                                            <div class="row mb-2">
                                                                <div class="col-md-2 font-bold-weight"
                                                                     style="font-weight: bold">
                                                                    การตรวจคะแนน :
                                                                </div>
                                                                <div class="col-md-9 text-left">
                                                                    <?php echo $data->question_standard == $RESULT_ONLY_VALUE ? $RESULT_ONLY_MSG : $data->question_standard == $RESULT_QUERY_VALUE ? $RESULT_QUERY_MSG : '' ?>
                                                                </div>
                                                            </div>

                                                            <div class="row mb-2">
                                                                <div class="col-md-2 font-bold-weight"
                                                                     style="font-weight: bold">
                                                                    คำตอบ :
                                                                </div>
                                                                <div class="col-md-9 text-left ow-break-word">
                                                                    <?php echo $data->question_answer ?>
                                                                </div>
                                                            </div>

                                                            <div class="row mb-2">
                                                                <div class="col-md-2 font-bold-weight"
                                                                     style="font-weight: bold">
                                                                    คะแนน :
                                                                </div>
                                                                <div class="col-md-9 text-left">
                                                                    <?php echo $data->question_point ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="text-center mt-5">
                                                            <button type="submit" name="form" value="2"
                                                                    class="btn btn-success mr-auto">ถัดไป
                                                            </button>
                                                            <button type="button" class="btn btn-danger ml-auto"
                                                                    data-dismiss="modal">ยกเลิก
                                                            </button>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <nav aria-label="Page navigation example border">
                <ul class="pagination justify-content-end">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="add_test_1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="background: #ce9f8d">
                <form method="post" action="./test.php?num=<?= isset($_GET['num']) ? $_GET['num'] : 1 ?>&show=1">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div style="min-height: 300px">
                            <h3>+ เพิ่มแบบทดสอบ</h3>


                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    ชื่อแบบทดสอบ
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="question_name" class="form-control col-md-6"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    ฐานข้อมูล
                                </div>
                                <div class="col-md-9">
                                    <select name="question_database" class="form-control col-md-10">
                                        <?php
                                        $curl = curl_init();

                                        curl_setopt_array($curl, array(
                                            CURLOPT_URL => $BASE_API_PATH . '/databases',
                                            CURLOPT_RETURNTRANSFER => true,
                                            CURLOPT_ENCODING => '',
                                            CURLOPT_MAXREDIRS => 10,
                                            CURLOPT_TIMEOUT => 0,
                                            CURLOPT_FOLLOWLOCATION => true,
                                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST => 'GET',
                                        ));

                                        $response = curl_exec($curl);

                                        curl_close($curl);
                                        //echo $response;
                                        $objParser = json_decode($response);
                                        $obj = $objParser->results;
                                        foreach ($obj as $data) {
                                            ?>
                                            <option value="<?= $data ?>"><?= $data ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    จำนวนข้อ
                                </div>
                                <div class="col-md-9">
                                    <input type="number" min="1" max="999"
                                           value="<?= isset($_GET['num']) ? $_GET['num'] : 1 ?>" name="question_num"
                                           class="form-control col-md-10"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    คำถามแบบ
                                </div>
                                <div class="col-md-9">
                                    <select name="question_type" class="form-control col-md-10">
                                        <option value="S">แบบกำหนดเอง</option>
                                        <option value="A">แบบอัตโนมัติ</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    วันที่กำหนดส่ง
                                </div>
                                <div class="col-md-9">
                                    <input type="date" name="question_end_date" class="form-control col-md-6"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    เวลาเริ่ม
                                </div>
                                <div class="col-md-2">
                                    <select name="start_time_h" class="form-control col-md-12">
                                        <?php
                                        for ($i = 0; $i < 24; ++$i) {
                                            ?>
                                            <option><?= $i < 10 && $i >= 0 ? "0" . $i : $i ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                                <B class="col-form-label"> : </B>
                                <div class="col-md-2">
                                    <select name="start_time_m" class="form-control col-md-12">
                                        <?php
                                        for ($i = 0; $i < 60; ++$i) {
                                            ?>
                                            <option><?= $i < 10 && $i >= 0 ? "0" . $i : $i ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    เวลาส่ง
                                </div>
                                <div class="col-md-2">
                                    <select name="end_time_h" class="form-control col-md-12">
                                        <?php
                                        for ($i = 0; $i < 24; ++$i) {
                                            ?>
                                            <option><?= $i < 10 && $i >= 0 ? "0" . $i : $i ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                                <B class="col-form-label"> : </B>
                                <div class="col-md-2">
                                    <select name="end_time_m" class="form-control col-md-12">
                                        <?php
                                        for ($i = 0; $i < 60; ++$i) {
                                            ?>
                                            <option><?= $i < 10 && $i >= 0 ? "0" . $i : $i ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    สถานะ
                                </div>
                                <div class="col-md-9">
                                    <select name="question_status" class="form-control col-md-4">
                                        <option value="1">เปิด</option>
                                        <option value="0">ปิด</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="text-center mt-5">
                            <button type="submit" name="form" value="2" class="btn btn-success mr-auto">ถัดไป</button>
                            <button type="button" class="btn btn-danger ml-auto" data-dismiss="modal">ยกเลิก</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--    FORM 2   -->
    <div class="modal fade" id="add_test_2S" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="background: #ce9f8d">
                <form method="post" action="./test.php">
                    <input type="hidden" name="req" value="<?php echo json_encode($req) ?>">
                    <input type="hidden" name="question_skill_type" value="">
                    <input type="hidden" name="question_sql_type" value="">
                    <input type="hidden" name="question_num_n"
                           value="<?php echo isset($_GET['num']) ? $_GET['num'] : 1 ?>">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div id="add_test_content_S" style="min-height: 300px">
                            <h3>+ เพิ่มแบบทดสอบกำหนดเอง</h3>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    โจทย์ข้อ <?= isset($_GET['num']) ? $_GET['num'] : 1 ?>
                                </div>
                                <div class="col-md-9">
                                    <textarea name="question_title" class="form-control col-md-10"></textarea>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    คำตอบข้อ <?= isset($_GET['num']) ? $_GET['num'] : 1 ?>
                                </div>
                                <div class="col-md-7">
                                    <textarea name="question_answer" id="question_answer"
                                              class="form-control col-md-12"></textarea>
                                </div>

                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary" onclick="onCLickQuery()" id="run_sql">
                                        ทดสอบ
                                    </button>
                                </div>
                            </div>

                            <div class="row mb-2" style="display: none" id="result_query">
                                <div class="col-md-2" style="color: darkblue">ผลลัพธ์</div>
                                <div class="col-md-9">
                                    <div class="col-md-12 overflow-auto"
                                         style="min-height: 250px;max-height: 250px;background: white;overflow-y: auto;">
                                        <table border="1">
                                            <thead style="background: white">
                                            <tr id="thead_header">
                                            </tr>
                                            </thead>
                                            <tbody id="tbody_content">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    คะแนน
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="question_point" class="form-control col-md-6"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    เกณฑ์
                                </div>
                                <div class="col-md-9">
                                    <select name="question_standard" class="form-control col-md-6">
                                        <option value="<?= $RESULT_ONLY_VALUE ?>">ผลลัพธ์เท่านั้น</option>
                                        <option value="<?= $RESULT_QUERY_VALUE ?>">คำสั่งและผลลัพธ์</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="text-center mt-5">
                            <!--                            <input  type="hidden">-->
                            <button type="submit" name="form" value="success" class="btn btn-success mr-auto">
                                สร้างแบบทดสอบ
                            </button>
                            <button type="button" class="btn btn-danger ml-auto" data-dismiss="modal">ยกเลิก</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--    FORM 3   -->
    <div class="modal fade" id="add_test_2A" tabindex="-1" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="background: #ce9f8d">
                <form method="post" action="./test.php">
                    <input type="hidden" name="req" value="<?php echo json_encode($req) ?>">
                    <input type="hidden" name="question_title" value="">
                    <input type="hidden" name="question_answer" value="">
                    <input type="hidden" name="req" value="<?php echo json_encode($req) ?>">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div style="min-height: 300px">
                            <h3>+ เพิ่มแบบทดสอบแบบอัตโนมัติ</h3>


                            <div class="row mb-2">
                                <div class="col-md-2">
                                    ชื่อแบบทดสอบ
                                </div>

                                <div class="col-md-9">
                                    <input type="radio" name="question_sql_type" value="INSERT" checked/>INSERT
                                    <input type="radio" name="question_sql_type" value="UPDATE"/>UPDATE
                                    <input type="radio" name="question_sql_type" value="SELECT"/>SELECT
                                    <input type="radio" name="question_sql_type" value="DELETE"/>DELETE
                                </div>

                            </div>


                            <div class="row mb-2">
                                <div class="col-md-2">
                                    ระดับคำถาม
                                </div>

                                <div class="col-md-9">
                                    <input type="radio" name="question_skill_type" value="REMEMBER" checked/>ความจำ
                                    <input type="radio" name="question_skill_type" value="UNDERSTAND"/>ความเข้าใจ
                                    <input type="radio" name="question_skill_type" value="IMPLEMENT"/>ประยุกต์
                                    <input type="radio" name="question_skill_type" value="ANALYSIS"/>วิเคราะห์
                                </div>

                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    คะแนน
                                </div>
                                <div class="col-md-9">
                                    <input type="text" name="question_point" class="form-control col-md-6"/>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-2 col-form-label">
                                    เกณฑ์
                                </div>
                                <div class="col-md-9">
                                    <select name="question_standard" class="form-control col-md-6">
                                        <option value="<?= $RESULT_ONLY_VALUE ?>">ผลลัพธ์เท่านั้น</option>
                                        <option value="<?= $RESULT_QUERY_VALUE ?>">คำสั่งและผลลัพธ์</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="text-center mt-5">
                            <!--                            <input  type="hidden">-->
                            <button type="submit" name="form" value="success"
                                    class="btn btn-success mr-auto">สร้างคำถาม
                            </button>
                            <button type="button" class="btn btn-danger ml-auto" data-dismiss="modal">ยกเลิก</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<?php include '../layout/footer.php' ?>
<script>
    if ("<?= isset($_GET['num']) ? $_GET['num'] : 0 ?>" > 0 && "<?= isset($_GET['show']) ? $_GET['show'] : 0 ?>" == 0) {
        let button = document.getElementById('add_test_2');
        button.click();

    }
    if (<?= isset($_POST['form']) ? $_POST['form'] : 0 ?> == 2) {
        //alert("<?= isset($_POST['form']) ? $_POST['question_type'] : 0 ?>")
        if ("<?= isset($_POST['form']) ? $_POST['question_type'] : 0 ?>" == "S") {
            let button = document.getElementById('add_test_2');
            button.click();

        }

        if ("<?= isset($_POST['form']) ? $_POST['question_type'] : 0 ?>" == "A") {
            let button = document.getElementById('add_test_3');
            button.click();
        }


    }

    function onClickSearch() {
        let textEl = document.getElementById("textSearch");
        let textSearch = textEl.value;
        window.location.href = "./test.php?search=" + textSearch;

    }

    function onCLickQuery() {

        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        let sqlEl = document.getElementById('question_answer');
        let sql = sqlEl.value

        let raw = JSON.stringify({
            "sql": sql
        });

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("<?php echo $BASE_API_PATH?>/query", requestOptions)
            .then(response => response.text())
            .then(result => {
                let resultBox = document.getElementById('result_query');
                let thead = document.getElementById('thead_header');
                let tbody = document.getElementById('tbody_content');
                resultBox.style.display = ''
                let html = "";
                let obj = JSON.parse(result);
                let data = obj.results.data;
                if (data.length > 0) {
                    let keys = Object.keys(data[0]);
                    keys.forEach((val, index) => {
                        html += `<th>`;
                        html += val;
                        html += `</th>`;
                    })
                    thead.innerHTML = html
                    html = ""
                    data.forEach((val, index) => {
                        html += `<tr>`;
                        keys.forEach((key) => {
                            html += `<td>`;
                            html += val[key];
                            html += `</td>`;
                        })
                        html += `</tr>`;
                    })
                    tbody.innerHTML = html
                }

            })
            .catch(error => console.log('error', error));

    }
</script>
</body>
</html>