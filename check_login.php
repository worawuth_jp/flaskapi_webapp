<?php
require './constants.php';
session_start();
$curl = curl_init();

$body = array();
$body['username'] = $_POST['username'];
$body['password'] = $_POST['password'];
$body['type_user'] = $_POST['type_user'];

curl_setopt_array($curl, array(
    CURLOPT_URL => $BASE_API_PATH.'/user/login',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => json_encode($body),
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
));

$response = curl_exec($curl);

curl_close($curl);

$res = json_decode($response);
print_r($res);
if(!$res->results->isLogin){
    header('location: ./login.php?error=1');
}
$_SESSION['type_user'] = $res->results->type_user;
switch ($_SESSION['type_user']){
    case $TYPE_ADMIN :
        $_SESSION['user_id'] = $res->results->user_id;
        header('location: ./admin');
        break;
    case $TYPE_STUDENT :
        $_SESSION['user_id'] = $res->results->user_id;

        header('location: ./students');
        break;
    case $TYPE_TEACHER:
        $_SESSION['user_id'] = $res->results->user_id;
        $_SESSION['subject_id'] = $res->results->subject_id;
        header('location: ./teachers');
        break;
}